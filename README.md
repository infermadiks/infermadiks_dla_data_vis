# README #


### What is this repository for? ###

* This repo is for code that cleans and reshapes excel spreadsheet data into a long format, needed for visualisation in Tableau.
* Version 1.1
* The code is run in R, using RStudio as a preference for running the R markdown script.

### How do I get set up? ###

* Requirements: R (Version 3.3.1 (2016-06-21) -- "Bug in Your Hair"), RStudio (Version 0.99.902)
* Configuration: Install packages if not already installed in R path by uncommenting the lines of code in the file.
* Dependencies: Base R packages
* How to run the script: Download the files or clone the repo. Set your working directory in R to the same folder as the data is saved in. Should the data be in a different folder, adjust your file path to the data accordingly.
* Change the path to the file to be saved according to your needs.

### Who do I talk to? ###

* Repo owner or admin: http://infermadiks.com/contact/
* Other community or team contact: theiligers@infermadiks.com