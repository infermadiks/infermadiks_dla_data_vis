## Setting working directory and installing packages needed:

setwd("/Users/tina/Projects/Infermadiks_DLA_data_vis")
#install.packages("knitr", repos='http://cran.us.r-project.org')
library("knitr")
#install.packages("readxl", repos='http://cran.us.r-project.org')
library("readxl")
#install.packages("reshape2", repos='http://cran.us.r-project.org')
library("reshape2")
#install.packages("WriteXLS", repos='http://cran.us.r-project.org')
library("WriteXLS")
#install.packages("stringi")
library("stringi")

##Getting initial data
#Character vector for column names
names = c("name", "theme1", "theme2", "theme3", "theme4", "theme5", "noData")
#Use read_excel to read the data into a data fram in R:
raw_data <- read_excel("DLA.xlsx", col_names = names, skip = 2)
#removing last column of no data
data <- raw_data[,1:6]
str(data)
head(data)

##Cleaning data:
#removing names, replacing them with "ID+random string of length3":
random_string_names <- paste("ID", stri_rand_strings(n=nrow(data), length=3, pattern="[A-Za-z0-9]"), sep="")
renamed <- unlist(random_string_names)
renamed
data$name <- renamed
head(data, 10)
data_copy <- data

#subset the name column keeping only data that doesn't have Na's

#removing rows with no values:
rows_to_remove <- which(complete.cases(data_copy) == FALSE)
str(rows_to_remove)
test_remove_Na_rows <- data_copy[-rows_to_remove,]
head(test_remove_Na_rows, 10)
data_needed <- test_remove_Na_rows

#spliting the data into groups (generate a list of sub-data frames for each group)
rows_to_split_on <- which(data_needed$theme1 == "Theme 1")
rows_to_split_on

#create a list of the smaller data frames, each having the first row containing original sub-setted column names:
data_split <- split(data_needed, cumsum(1:nrow(data_needed) %in% rows_to_split_on))
str(data_split)
head(data_split)
#str(data_split[1])
#head(data_split[1])
#str(data_split[[1]])               
#head(data_split[[1]])

##Removing first row of data from each small dataframe and making a new list of df's from that:
data_split_top_rows_removed <- data_split
for(i in (1:length(data_split))) {
    data_split_top_rows_removed[[i]] <- data_split[[i]][2:nrow(data_split[[i]]),]
    }
head(data_split_top_rows_removed)

##Reshaping the data into long format
melt_data <- lapply(data_split_top_rows_removed, melt, id = "name")
head(melt_data, 3)
 
##creating a vector of group names for each data frame:
 
group_names <- list()
group_names <- paste("group", 1:10, sep="")
str(group_names)
 
##adding these to each data frame in the list of data frames:
melt_data_with_group_numbers <- melt_data
for (i in 1:10){
     melt_data_with_group_numbers[[i]]$group <- group_names[i]
    }
df_for_groups <- melt_data_with_group_numbers

##Adding the domains to the data that the strengths themes correspond with:
###All this is in a loop to go through each data frame in the list:


for (i in 1:length(df_for_groups)){
    #subset of the list of data frames to work on one at a time:
    #df_for_groups[[i]] <- df_for_groups[[1]]
    df_for_groups[[i]]

    #converting the variable column entries to characters entries:
    df_for_groups[[i]]$variable <- as.character(df_for_groups[[i]]$variable)
    str(df_for_groups[[i]]$variable)

    #renaming theme(x) in variable to appropriate domain using category in value column
    Execution <- c("Achiever", "Arranger", "Belief", "Consistency", "Deliberative", "Discipline", "Focus", "Responsibility", "Restorative")
    Strategic <- c("Analytical", "Context", "Futuristic", "Ideation", "Input", "Intellection", "Learner", "Strategic")
    Influencing <- c("Activator","Command", "Communication", "Competition", "Maximizer", "Self-Assurance", "Significance", "Woo")
    Relationship <- c("Adaptability", "Developer", "Connectedness", "Empathy", "Harmony", "Includer", "Individualization", "Positivity", "Relator")

    Execution_rows <- which(df_for_groups[[i]]$value %in% Execution)
    Strategic_rows <- which(df_for_groups[[i]]$value %in% Strategic)
    Influencing_rows <- which(df_for_groups[[i]]$value %in% Influencing)
    Relationship_rows <- which(df_for_groups[[i]]$value %in% Relationship)

    #replacing the "theme(x)" entries with donamins:
    df_for_groups[[i]]$variable[Execution_rows] <- "Execution"
    df_for_groups[[i]]$variable[Strategic_rows] <- "Strategic"
    df_for_groups[[i]]$variable[Influencing_rows] <- "Influencing"
    df_for_groups[[i]]$variable[Relationship_rows] <- "Relationship"
    df_for_groups[[i]]
}

head(df_for_groups, 3)
 
##recombining all the data frames into one:
 
data_tableau_ready <- do.call("rbind", df_for_groups)
head(data_tableau_ready)

# #Saving the resultant data frame as a Tableau-friendly Excel file:
# 
# #saving data to .xls
WriteXLS(data_tableau_ready, ExcelFileName = "DLA_Tableau_ready.xls", row.names = FALSE, col.names = FALSE)
